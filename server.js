'use strict';

const express = require('express');

// Constantes
const PORT = 8081;
const path = require('path');

// App
const app = express();

app.get('/',function(req,res) {
//  res.send("Bienvenido terrícola \n");
res.sendFile(path.join(__dirname+'/index.html'));

});

app.listen(PORT);
console.log('Express funcionando en el puerto' + PORT);